#!/bin/bash

echo "Logging in"
docker login registry.gitlab.com

echo "Building python3 image"
docker build -t registry.gitlab.com/kint.jonas/minipy:2.7 -f Dockerfile2 .
docker build -t registry.gitlab.com/kint.jonas/minipy:3.5 -f Dockerfile3 .

echo "Pushing images"
docker push registry.gitlab.com/kint.jonas/minipy:2.7
docker push registry.gitlab.com/kint.jonas/minipy:3.5

exit 0